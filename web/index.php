<?php


require __DIR__ . '/../config/config.php';

$handled_files = 0;
$file_amount = 0;
$form_uploaded = false;
$simple_output = false;

if (($_SERVER['REQUEST_METHOD'] === 'POST') && isset($_POST['upload'])) {
    $form_uploaded = true;
    $file_amount = count($_FILES['files']['name']);
    if (isset($_POST['simple_output'])) {
        $simple_output = true;
    }

    //Handle the file upload:
    foreach ($_FILES['files']['name'] as $index => $file_name) {
        if (!$file_name) {
            //A file without file name will not be processed.
            continue;
        }
        if ($_FILES['files']['size'] < 1) {
            //We don't need to handle empty files.
            continue;
        }
        if ($_FILES['files']['error'] != UPLOAD_ERR_OK) {
            //We don't hanlde uploads that created an error.
            continue;
        }

        $cleaned_file_name = trim(
            str_replace(
                ['/', '../', './', '\\', '\'', '"'],
                '',
                $file_name

            )
        );

        if (!$cleaned_file_name) {
            //Use the md5 of the temporary file name with the current timestamp
            //and a random value as file name:
            $cleaned_file_name = md5(
                $_FILES['files']['tmp_name'][$index]
              . time()
              . rand(1, PHP_INT_MAX)
            );
        }

        move_uploaded_file(
            $_FILES['files']['tmp_name'][$index],
            $uload_dir . '/' . $cleaned_file_name
        );

        $handled_files++;
    }
}

require __DIR__ . '/../config/form.php';
