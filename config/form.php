<!DOCTYPE html>
<html>
    <head>
        <title>tiny-upload-form</title>
    </head>
    <body>
        <form method="post">
            <label for="files">
                <?= htmlspecialchars($upload_file_message) ?>
            </label>
            <input type="file" name="files" multiple="multiple">
            <input type="submit" name="upload"
                   value="<?= htmlspecialchars($submit_button_text) ?>">
        </form>
    </body>
</html>
