<?php


/*
 * NOTE: The temporary directory for uploaded files can be set
 * using a .htaccess file in the web directory.
 * Specify the upload directory using the following directive:
 * php_value upload_tmp_dir $YOUR_TMP_DIR
 *
 * You may also want to set other directives in the .htaccess file
 * like the maximum size of an uploaded file, specifying a password
 * to limit access etc.
 */


/*
 * The amount of files that can be uploaded simultaneously.
 */
$max_simultaneous_uploads = 10;

/*
 * The message that shall be displayed above the file input.
 */
$upload_file_message = 'Upload one or multiple files here.';


/*
 * The text for the submit button.
 */
$submit_button_text = 'Upload';
